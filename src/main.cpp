//рус

#include <stdio.h>
#include "SEGGER_RTT.h"

#include "config.h"
#include "RccHandle.h"
#include "AdcHandle.h"
#include "GpioHandle.h"
#include "TimHandle.h"
#include "UartHandle.h"
#include "I2cHandle.h"
#include "ALX_TIM.hpp"

Gpio::PinDef RedLed1    = {GPIOC, 3};
Gpio::PinDef GreenLed1  = {GPIOC, 2};
Gpio::PinDef BlueLed1   = {GPIOC, 1};
Gpio::PinDef RedLed2    = {GPIOC, 15};
Gpio::PinDef GreenLed2  = {GPIOC, 14};
Gpio::PinDef BlueLed2   = {GPIOC, 13};

MessageBufferHandle_t uartTxMessageBuffer;

TaskHandle_t mainTask;
TaskHandle_t uartTransferTask;
TaskHandle_t i2cMasterTask;
TaskHandle_t adcConvertTask;
TaskHandle_t adcVoltTask;

SemaphoreHandle_t uartTxSemaphore;

static void MainTaskFunc(void* pvParameters);

int main()
{
	ClockInit();
	GpioInit();
	TimInit();
	FRTOS_TaskCreateStatic(MainTaskFunc, "Main", 256, nullptr, FRTOS_PRIORITY_1, mainTask);
	Tim1.CountEnable();
	vTaskStartScheduler();
	while(1){}
}

static void MainTaskFunc(void* pvParameters)
{
	IWDG->KR = 0x5555;
	IWDG->PR = 3;
	IWDG->KR = 0x5555;
	IWDG->RLR = 2500;
	IWDG->KR = 0xCCCC;

	FRTOS_MessageBufferCreateStatic(1024, uartTxMessageBuffer);
	FRTOS_SemaphoreCreateBinaryStatic(uartTxSemaphore);//uartTxSemaphore

	vTaskSuspendAll();
		FRTOS_TaskCreateStatic(UartTransferTaskFunc, "uartTx", 1024, nullptr, FRTOS_PRIORITY_1, uartTransferTask);
		FRTOS_TaskCreateStatic(I2cMasterTaskFunc,    "i2cMs",  1024, nullptr, FRTOS_PRIORITY_2, i2cMasterTask);
		FRTOS_TaskCreateStatic(AdcConvertTaskFunc,   "Adc",    1024, nullptr, FRTOS_PRIORITY_3, adcConvertTask);
		FRTOS_TaskCreateStatic(AdcVoltTaskFunc,   "AdcV",    1024, nullptr, FRTOS_PRIORITY_2, adcVoltTask);
	xTaskResumeAll();

	auto lastWakeTime = xTaskGetTickCount();
	uint8_t tickCounter = 0;
	for(;;)
	{
		IWDG->KR = 0xAAAA;//update
		GreenLed1.InvertPin();
		if(tickCounter > 60)
		{
			FrtosGetTaskInfo(mainTask);
			FrtosGetTaskInfo(uartTransferTask);
			FrtosGetTaskInfo(i2cMasterTask);
			FrtosGetTaskInfo(adcConvertTask);
			FrtosGetTaskInfo(xTaskGetIdleTaskHandle());
			tickCounter = 0;
		}
		else ++tickCounter;

		vTaskDelayUntil(&lastWakeTime, 1000);
	}
}




