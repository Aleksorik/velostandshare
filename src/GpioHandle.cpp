
#include "GpioHandle.h"
#include "stm32f1xx_alx_gpio.hpp"
#include "stm32f1xx_ll_gpio.h"

using namespace Gpio;

void GpioInit()
{
	LL_GPIO_AF_Remap_SWJ_NOJTAG();
	GPIO_SetOver(GpioA,
		(Pin(0,3)            << Output::GPPushPull_SpeedHigh)| //Trace tasks
		(Pin(4)/*ADC1_CH4*/  << Gpio::Input::Analog)|
		(Pin(5)/*ADC1_CH5*/  << Gpio::Input::Analog)|
		//(Pin(5)/*ADC1_CH5*/  << Input::Pull << Pull::Up)|
		(Pin(9)              << Output::AFPushPull_SpeedHigh)| //USART1TX
		(Pin(10)             << Input::Pull << Pull::Up)|     //USART1RX
		(Pin(11)             << Input::Floating)|             //CAN1RX
		(Pin(12)             << Output::AFPushPull_SpeedHigh) //CAN1TX
	);

	GPIO_SetOver(GpioB,
		(Pin(6) /*BUTTON*/        << Input::Pull << Pull::Up)|
		(Pin(8) /*I2C1_SCL*/      << Output::AFOpenDrain_SpeedHigh)|//left
		(Pin(9) /*I2C1_SDA*/      << Output::AFOpenDrain_SpeedHigh)|
		(Pin(10)/*I2C2_SCL*/      << Output::AFOpenDrain_SpeedHigh)|//right
		(Pin(11)/*I2C2_SDA*/      << Output::AFOpenDrain_SpeedHigh)|
		(Pin(12)/*CAN2RX*/        << Input::Floating)|
		(Pin(13)/*CAN2TX*/        << Output::AFPushPull_SpeedHigh)
	);
	LL_GPIO_AF_EnableRemap_I2C1();

	GPIO_SetOver(GpioC,
		(Pin(1,3)/*LED*/ 	<< Output::GPPushPull_SpeedHigh)|//LED
		(Pin(6)  /*ALERT*/ << Input::Pull << Pull::Up)|
		(Pin(13,15)/*LED*/ << Output::GPPushPull_SpeedHigh)//LED
	);
	LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTC, LL_GPIO_AF_EXTI_LINE6);

//	GpioB.BSRR = (uint32_t)(GPIO_PIN_10|GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_15);
//	GpioC.BSRR = (uint32_t)GPIO_PIN_4;
//	GpioA.BRR = GPIO_PIN_3;
}
