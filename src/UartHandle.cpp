#include "UartHandle.h"

#include "config.h"
#include "stm32f1xx_ll_usart.h"

#define LOG_MODULE_NAME uart
#if defined(LOG_LEVEL_UART)
	#define LOG_MODULE_LEVEL LOG_LEVEL_UART
#endif
#include "log_utils.h"

static void UartInit(uint32_t inClk, uint32_t inBaudrate)
{
	auto& usart = Usart1;
	//Nvic::SetPriority(DMA_UartRx_IRQn, 5, 0);
	//NVIC_EnableIRQ(DMA_UartRx_IRQn);
	Nvic::SetPriority(DMA_UartTx_IRQn, 5, 0);
	NVIC_EnableIRQ(DMA_UartTx_IRQn);

	USART_SetOver(usart, Usart::Init()
		<< Usart::WordLengh::bit8
		<< Usart::Stop::s1
		<< Usart::Dma::TxOnly
	);

	usart.SetPrescaler(inClk/inBaudrate);
	usart.RxTxEnable();

//	DMA_SetOver(DmaUartRx, Dma::Init()
//		<< Dma::DataSize::Mem8Per8
//		<< Dma::IncrementMode::MInc
//		<< Dma::CircularMode::Disable
//		<< Dma::DataTransferDirection::PerToMem
//		<< Dma::Interrupt::TransferComplete::Enable
//	);
//	DmaUartRx.SetPeripheralAddress(&usart.DR);

	DMA_SetOver(DmaUartTx, Dma::Init()
		<< Dma::DataSize::Mem8Per8
		<< Dma::IncrementMode::MInc
		<< Dma::CircularMode::Disable
		<< Dma::DataTransferDirection::MemToPer
		<< Dma::Interrupt::TransferComplete::Enable
	);
	DmaUartTx.SetPeripheralAddress(&usart.DR);
}

extern SemaphoreHandle_t uartTxSemaphore;
void UartTransferTaskFunc(void* pvParameters)
{
	UartInit(Apb2ClockHz, 115'200);
	constexpr uint32_t TX_BUFFER_SIZE = 64;
	static uint8_t txBuffer[TX_BUFFER_SIZE];
	DmaUartTx.SetMemoryAddress(txBuffer);
	for(;;)
	{
		uint32_t size = xMessageBufferReceive(uartTxMessageBuffer, txBuffer, TX_BUFFER_SIZE, portMAX_DELAY);
		DmaUartTx.SetNumberOfData(size);
		DmaUartTx.Enable();
		uint32_t notifyFlag = UART_NOTIFY_NONE;
		xSemaphoreTake(uartTxSemaphore, portMAX_DELAY);
		LOG_DEVELOP("Uart tx");
	}
}

