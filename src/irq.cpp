#include "irq.h"
#include "config.h"

#include "freertos_utils.h"
#include "ALX_TIM.hpp"
#include "ALX_I2C.hpp"
#include "stm32f1xx_alx_dma.hpp"
#include "I2cHandle.h"
#include "AdcHandle.h"
#include "UArtHandle.h"
#include "stm32f1xx_ll_adc.h"
#include "stm32f1xx_ll_gpio.h"
#include "stm32f1xx_ll_exti.h"

extern "C" void vApplicationTickHook( void )
{
	Tim1.ClearFlagUpdate();
}

extern SemaphoreHandle_t uartTxSemaphore;
extern "C" void DMA1_Channel4_IRQHandler(void)
{//rx
  static BaseType_t taskWoken;
  taskWoken = pdFALSE;
	static auto& DmaX = Dma1_Channel4;
	if(DmaX.IsFlagComplete())
	{
		DmaX.ClearFlagComplete();
		DmaX.Disable();
		xSemaphoreGiveFromISR(uartTxSemaphore, &taskWoken);
	}
	portYIELD_FROM_ISR(taskWoken);
}

extern bool isDirectionI2cMsTx;
extern TaskHandle_t i2cMasterTask;

extern "C" void EXTI9_5_IRQHandler(void)
{
	static BaseType_t taskWoken;
	taskWoken = pdFALSE;
	if(LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_6))
	{
		LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_6);
		xTaskNotifyFromISR(i2cMasterTask, GPIO_NOTIFY_ALERT, eSetBits, &taskWoken);
	}
	portYIELD_FROM_ISR(taskWoken);
}

extern "C" void DMA1_Channel5_IRQHandler(void)
{//tx
	static auto& DmaX = Dma1_Channel5;
	if(DmaX.IsFlagComplete())
	{
		DmaX.ClearFlagComplete();
		DmaX.Disable();
	}
}


extern "C" void I2C1_ER_IRQHandler(void)
{
	static BaseType_t taskWoken;
	taskWoken = pdFALSE;
	static auto& I2cX = i2cMs;
	if(I2cX.IsFlagAckFailed())
	{
		I2cX.ClearAckFailed();
		xTaskNotifyFromISR(i2cMasterTask, I2C_NOTIFY_ACK_FAILED, eSetBits, &taskWoken);
	}
	else if(I2cX.IsFlagBusError())
	{
		I2cX.ClearBusError();
		xTaskNotifyFromISR(i2cMasterTask, I2C_NOTIFY_BUS_ERR, eSetBits, &taskWoken);
	}
	else if(I2cX.IsFlagOverUnderRun())
	{
		I2cX.ClearOverUnderRun();
	}
	else if(I2cX.IsFlagArbitrationLost())
	{
		I2cX.ClearArbitrationLost();
	}
	//SMBus
//	else if(I2cX.IsFlagPecError())
//	{
//		I2cX.ClearPecError();
//	}
//	else if(I2cX.IsFlagTimoutError())
//	{
//		I2cX.ClearTimoutError();
//	}
//	else if(I2cX.IsFlagAlert())
//	{
//		I2cX.ClearFlagAlert();
//	}
	portYIELD_FROM_ISR(taskWoken);
}

extern "C" void I2C1_EV_IRQHandler(void)
{
	static BaseType_t taskWoken;
	taskWoken = pdFALSE;
	static auto& I2cX = i2cMs;
	if(I2cX.IsFlagAddress())
	{
		I2cX.ClearFlagAddress();
		xTaskNotifyFromISR(i2cMasterTask, I2C_NOTIFY_ADDRESS, eSetBits, nullptr);
	}
	else if(I2cX.IsFlagByteTxFinished())
	{
		I2cX.ClearFlagByteTxFinished();
		if(isDirectionI2cMsTx)
		{
			I2cX.Stop();
			xTaskNotifyFromISR(i2cMasterTask, I2C_NOTIFY_TX, eSetBits, &taskWoken);
		}
		else
		{
			xTaskNotifyFromISR(i2cMasterTask, I2C_NOTIFY_TX_FINISHED, eSetBits, &taskWoken);
		}
	}
	else if(I2cX.IsFlagStart())
	{
		if(isDirectionI2cMsTx)
		{
			I2cX.SendAddressTx(i2cAddress);
		}
		else
		{
			I2cX.SendAddressRx(i2cAddress);
		}
		xTaskNotifyFromISR(i2cMasterTask, I2C_NOTIFY_START, eSetBits, nullptr);
	}
	else if(I2cX.IsFlagStop())
	{
		I2cX.ClearFlagStop();
		if(isDirectionI2cMsTx)
		{
			dmaI2cMasterTx.Disable();
		}
		xTaskNotifyFromISR(i2cMasterTask, I2C_NOTIFY_STOP, eSetBits, &taskWoken);
	}
	portYIELD_FROM_ISR(taskWoken);
}

extern "C" void DMA1_Channel6_IRQHandler(void)
{
	static BaseType_t taskWoken;
	taskWoken = pdFALSE;
	static auto& DmaX = dmaI2cMasterTx;
	if(DmaX.IsFlagComplete())
	{
		DmaX.ClearFlagComplete();
		DmaX.Disable();
		//BlueLed2.SetPin();
		//xTaskNotifyFromISR(i2cMasterTask, I2C_NOTIFY_TX1, eSetBits, nullptr);
	}
	portYIELD_FROM_ISR(taskWoken);
}

extern "C" void DMA1_Channel7_IRQHandler(void)
{
	static BaseType_t taskWoken;
	taskWoken = pdFALSE;
	static auto& DmaX = dmaI2cMasterRx;
	if(DmaX.IsFlagComplete())
	{
		DmaX.ClearFlagComplete();
		DmaX.Disable();
		i2cMs.Stop();
		BlueLed2.SetPin();
		xTaskNotifyFromISR(i2cMasterTask, I2C_NOTIFY_RX, eSetBits, &taskWoken);
	}
	portYIELD_FROM_ISR(taskWoken);
}


extern TaskHandle_t adcConvertTask;
extern "C" void ADC_IRQHandler(void)
{
	if(LL_ADC_IsActiveFlag_AWD1(ADC1))
	{
		LL_ADC_ClearFlag_AWD1(ADC1);
		static bool isLowWd = false;
		if(isLowWd)
		{
			LL_ADC_SetAnalogWDThresholds(ADC1, LL_ADC_AWD_THRESHOLD_LOW, 0);
			LL_ADC_SetAnalogWDThresholds(ADC1, LL_ADC_AWD_THRESHOLD_HIGH, 120);
			GreenLed2.ResetPin();
		}
		else
		{
			LL_ADC_SetAnalogWDThresholds(ADC1, LL_ADC_AWD_THRESHOLD_LOW, 90);
			LL_ADC_SetAnalogWDThresholds(ADC1, LL_ADC_AWD_THRESHOLD_HIGH, 0xFFFFFF);
			xTaskNotifyFromISR(adcConvertTask, ADC_NOTIFY_AWD, eSetBits, nullptr);
			GreenLed2.SetPin();
		}
		isLowWd = !isLowWd;
	}
}

extern "C" void SysTick_Handler(void)
{

}


extern "C" void HardFault_Handler(void)
{
	RedLed2.SetPin();
	while(1);
}

signed char* ErrTask;

extern "C" void vApplicationStackOverflowHook( TaskHandle_t xTask, signed char* pcTaskName )
{
	RedLed2.SetPin();
	ErrTask = pcTaskName;
}

