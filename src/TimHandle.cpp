#include "TimHandle.h"
#include "config.h"
#include "ALX_TIM.hpp"

using namespace Tim;

void TimInit()
{
	Nvic::SetPriority(TIM1_UP_TIM10_IRQn, 15, 0);
	NVIC_EnableIRQ(TIM1_UP_TIM10_IRQn);

	TIM_SetClean(Tim1, Tim::Init(Channel(1,2))
		<< Tim::CounterMode::CenterAligned3
		<< ARPreload::Enable
		//<< Tim::CCMode::Output
		//<< Tim::Output::Preload::Enable
		//<< Tim::Output::Mode::Pwm1
		//<< Tim::Output::Enable
		//<< Tim::Output::MainEnable
		<< Tim::Interrupt::Update::Enable
	);
	Tim1.SetPrescaler(2-1);
	Tim1.SetAutoReload(Apb2TimHz/1000/2-1);
	//Tim1.SetCC1(18000);
	//Tim1.SetCC2(1);
	TIM_SetOver(Tim1, Tim::Init()
	<< Tim::MasterMode::CompareOC2REF
	);
}


