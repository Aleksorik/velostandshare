
#include "RccHandle.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_ll_system.h"

void ClockInit()
{
	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
	LL_FLASH_EnablePrefetch();

	RCC_SetOverSysClk(
		Rcc::Prediv1::SetDiv(2) <<
		Rcc::PllSource::Prediv1 <<
		Rcc::Pll::SetMul(9)     <<
		Rcc::AhbPresc::Div1     <<
		Rcc::Apb1Presc::Div4    <<
		Rcc::Apb2Presc::Div1    <<
		Rcc::AdcPresc::Div6
	);

	Rcc::SysClk::PllHse();

	SystemCoreClockUpdate();

	//////////////////

	RCC_SetOverClkEnable(
		   Rcc::Clk::AlternateFunction::Enable
		<< Rcc::Clk::GpioA::Enable
		<< Rcc::Clk::GpioB::Enable
		<< Rcc::Clk::GpioC::Enable
		<< Rcc::Clk::GpioD::Enable
		<< Rcc::Clk::GpioE::Enable

		<< Rcc::Clk::Crc::Enable

		<< Rcc::Clk::Dma1::Enable
//		<< Rcc::Clk::Dma2::Enable

		<< Rcc::Clk::Spi1::Enable
//		<< Rcc::Clk::Spi2::Enable
//		<< Rcc::Clk::Spi3::Enable


		<< Rcc::Clk::I2c1::Enable
		<< Rcc::Clk::I2c2::Enable

		<< Rcc::Clk::Usart1::Enable

		<< Rcc::Clk::Tim1::Enable
		<< Rcc::Clk::Tim2::Enable
		<< Rcc::Clk::Tim3::Enable
		<< Rcc::Clk::Tim4::Enable
		<< Rcc::Clk::Tim5::Enable
		<< Rcc::Clk::Tim6::Enable
		<< Rcc::Clk::Tim7::Enable
//		<< Rcc::Clk::Tim16::Enable
		<< Rcc::Clk::Can1::Enable
		<< Rcc::Clk::Can2::Enable

		<< Rcc::Clk::I2c1::Enable
		<< Rcc::Clk::I2c2::Enable

		<< Rcc::Clk::Adc1::Enable
		//<< Rcc::Clk::Adc2::Enable
	);

  //HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);
  //HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
}











