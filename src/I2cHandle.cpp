#include <stdio.h>
#include "SEGGER_RTT.h"

#include "config.h"
#include "I2cHandle.h"
#include "AdcHandle.h"
#include "stm32f1xx_ll_exti.h"

#define LOG_MODULE_NAME "i2c"
#if defined(LOG_LEVEL_I2C)
	#define LOG_MODULE_LEVEL LOG_LEVEL_I2C
#endif
#include "log_utils.h"

bool isDirectionI2cMsTx = true;

enum Ads1115_ConfigE
{
	CONFIG_H_MUX_0and1        = 0b000<<4,
	CONFIG_H_MUX_0and3        = 0b001<<4,
	CONFIG_H_MUX_1and3        = 0b010<<4,
	CONFIG_H_MUX_2and3        = 0b011<<4,
	CONFIG_H_MUX_0gnd         = 0b100<<4,
	CONFIG_H_MUX_1gnd         = 0b101<<4,
	CONFIG_H_MUX_2gnd         = 0b110<<4,
	CONFIG_H_MUX_3gnd         = 0b111<<4,

	CONFIG_H_RANGE_6144mV     = 0b000<<1,
	CONFIG_H_RANGE_4096mV     = 0b001<<1,
	CONFIG_H_RANGE_2048mV     = 0b010<<1,
	CONFIG_H_RANGE_1024mV     = 0b011<<1,
	CONFIG_H_RANGE_512mV      = 0b100<<1,
	CONFIG_H_RANGE_256mV      = 0b101<<1,

	CONFIG_H_MODE_CONTINIOUS  = 0b0<<0,
	CONFIG_H_MODE_SINGLE      = 0b1<<0,


	CONFIG_L_RATE_8sps        = 0b000<<5,
	CONFIG_L_RATE_16sps       = 0b001<<5,
	CONFIG_L_RATE_32sps       = 0b010<<5,
	CONFIG_L_RATE_64sps       = 0b011<<5,
	CONFIG_L_RATE_125sps      = 0b100<<5,
	CONFIG_L_RATE_250sps      = 0b101<<5,
	CONFIG_L_RATE_475sps      = 0b110<<5,
	CONFIG_L_RATE_860sps      = 0b111<<5,

	CONFIG_L_COMP_QUE_1       = 0b00<<0,
	CONFIG_L_COMP_QUE_2       = 0b01<<0,
	CONFIG_L_COMP_QUE_4       = 0b10<<0,
	CONFIG_L_COMP_QUE_DISABLE = 0b11<<0

};

constexpr auto operator| (const Ads1115_ConfigE a, const Ads1115_ConfigE b)
{
	return (uint32_t)a|(uint32_t)b;
}

static constexpr uint32_t I2C_TIMEOUT = 50;
static constexpr uint32_t I2C_ADRESS = 0b1001010;

uint8_t i2cAddress = 0;

DATA_ALIGN_TO(sizeof(uint32_t)) uint8_t arrTxBufferMs[I2C_BUFFER_SIZE_TX] = {1,2,3,4,5};
DATA_ALIGN_TO(sizeof(uint32_t)) uint8_t arrRxBufferMs[I2C_BUFFER_SIZE_RX];

//static bool I2cMasterTx(uint16_t inSize, uint8_t inAddress);
//static bool I2cMasterRx(uint16_t inSize, uint8_t inAddress);


template<uint8_t printLevel = 0>
static bool I2cMasterTx(uint16_t inSize, uint8_t inAddress)
{
	//printf("%sTxAddress=%d, size=%d; ", RTT_CTRL_TEXT_MAGENTA, inAddress, inSize);
	i2cAddress = inAddress;
	isDirectionI2cMsTx = true;
	dmaI2cMasterTx.SetNumberOfData(inSize);
	dmaI2cMasterTx.Enable();
	i2cMs.Start();
	uint32_t notifyFlag = I2C_NOTIFY_NONE;
	xTaskNotifyWait(I2C_NOTIFY_NONE, I2C_NOTIFY_ALL, &notifyFlag, 2);
	if(notifyFlag&I2C_NOTIFY_START)
	{
		LOG_DEVELOP("START_0x%02X", notifyFlag);
		if(!(notifyFlag&I2C_NOTIFY_ADDRESS))
			xTaskNotifyWait(I2C_NOTIFY_NONE, I2C_NOTIFY_ALL, &notifyFlag, 5);
		if(notifyFlag&I2C_NOTIFY_ADDRESS)
		{
			LOG_DEVELOP("ADDRESS_0x%02X", notifyFlag);
			if(!(notifyFlag&I2C_NOTIFY_TX))
				xTaskNotifyWait(I2C_NOTIFY_NONE, I2C_NOTIFY_ALL, &notifyFlag, I2C_TIMEOUT);
			if(notifyFlag&I2C_NOTIFY_TX)
			{
				LOG_DEVELOP("TX_0x%02X", notifyFlag);
				return true;
			}
		}
	}
	if(notifyFlag&I2C_NOTIFY_ACK_FAILED)
		LOG_WARNING("ACK_FAILED_0x%02X", notifyFlag);
	if(notifyFlag&I2C_NOTIFY_STOP)
		LOG_WARNING("STOP_0x%02X", notifyFlag);
	if(notifyFlag&I2C_NOTIFY_BUS_ERR)
		LOG_ERROR("BUS_ERR_%0x%02X", notifyFlag);
	dmaI2cMasterTx.Disable();
	xTaskNotifyWait(I2C_NOTIFY_NONE, I2C_NOTIFY_ALL, &notifyFlag, 0);
	return false;
}

template<uint8_t printLevel = 0>
static bool I2cMasterRx(uint16_t inSize, uint8_t inAddress)
{
	//printf("%sRxAddress=%d, size=%d; ", RTT_CTRL_TEXT_MAGENTA, inAddress, inSize);
	i2cAddress = inAddress;
	isDirectionI2cMsTx = false;
	dmaI2cMasterRx.SetNumberOfData(inSize);
	dmaI2cMasterRx.Enable();
	i2cMs.Start();
	uint32_t notifyFlag = I2C_NOTIFY_NONE;
	xTaskNotifyWait(I2C_NOTIFY_NONE, I2C_NOTIFY_ALL, &notifyFlag, 2);
	if(notifyFlag&I2C_NOTIFY_START)
	{
		if constexpr(printLevel > 1) printf("%sSTART_%d; ", RTT_CTRL_TEXT_BLUE, notifyFlag);
		if(!(notifyFlag&I2C_NOTIFY_ADDRESS))
			xTaskNotifyWait(I2C_NOTIFY_NONE, I2C_NOTIFY_ALL, &notifyFlag, 5);
		if(notifyFlag&I2C_NOTIFY_ADDRESS)
		{
			if constexpr(printLevel > 1) printf("%sADDRESS_%d; ", RTT_CTRL_TEXT_BLUE, notifyFlag);
			if(!(notifyFlag&I2C_NOTIFY_RX))
				xTaskNotifyWait(I2C_NOTIFY_NONE, I2C_NOTIFY_ALL, &notifyFlag, I2C_TIMEOUT);
			if(notifyFlag&I2C_NOTIFY_RX)
			{
				if constexpr(printLevel > 1) printf("%sRX_%d:\n", RTT_CTRL_TEXT_GREEN, notifyFlag);
				if constexpr(printLevel > 0) printf("%s", RTT_CTRL_RESET);
				for(uint32_t i =0; i<inSize; ++i)
					if constexpr(printLevel > 0) printf("%d; ", arrRxBufferMs[i]);
				if constexpr(printLevel > 0) printf("\n");
				return true;
			}
		}
	}
	if(notifyFlag&I2C_NOTIFY_ACK_FAILED)
		if constexpr(printLevel > 1) printf("%sACK_FAILED_%d; ", RTT_CTRL_TEXT_YELLOW, notifyFlag);
	if(notifyFlag&I2C_NOTIFY_STOP)
		if constexpr(printLevel > 1) printf("%sSTOP_%d; ", RTT_CTRL_TEXT_RED, notifyFlag);
	if(notifyFlag&I2C_NOTIFY_BUS_ERR)
		if constexpr(printLevel > 1) printf("%sBUS_ERR_%d; ", RTT_CTRL_TEXT_RED, notifyFlag);
	if constexpr(printLevel > 1) printf("\n");
	dmaI2cMasterRx.Disable();
	xTaskNotifyWait(I2C_NOTIFY_NONE, I2C_NOTIFY_ALL, &notifyFlag, 0);
	return false;
}


static void I2cInit()
{
	Nvic::SetPriority(DMA1_Channel6_IRQn, 8, 0);//1Tx
	NVIC_EnableIRQ(DMA1_Channel6_IRQn);
	Nvic::SetPriority(DMA1_Channel7_IRQn, 9, 0);//1Rx
	NVIC_EnableIRQ(DMA1_Channel7_IRQn);

	DMA_SetOver(dmaI2cMasterTx, Dma::Init()
		<<Dma::DataSize::Mem8Per8
		<<Dma::IncrementMode::MInc
		<<Dma::Interrupt::TransferComplete::Enable
		<<Dma::DataTransferDirection::MemToPer
		<<Dma::CircularMode::Disable
	);
	dmaI2cMasterTx.SetMemoryAddress(arrTxBufferMs);
	dmaI2cMasterTx.SetPeripheralAddress(&i2cMs.DR);

	DMA_SetOver(dmaI2cMasterRx, Dma::Init()
		<<Dma::DataSize::Mem8Per8
		<<Dma::IncrementMode::MInc
		<<Dma::Interrupt::TransferComplete::Enable
		<<Dma::DataTransferDirection::PerToMem
		<<Dma::CircularMode::Disable
	);
	dmaI2cMasterRx.SetMemoryAddress(arrRxBufferMs);
	dmaI2cMasterRx.SetPeripheralAddress(&i2cMs.DR);

	Nvic::SetPriority(I2C1_ER_IRQn, 5, 0);
	NVIC_EnableIRQ(I2C1_ER_IRQn);
	Nvic::SetPriority(I2C1_EV_IRQn, 5, 0);
	NVIC_EnableIRQ(I2C1_EV_IRQn);
	i2cMs.Reset();
	I2C_SetOver(i2cMs, I2c::Init()
		<< I2c::SetClockDividerFastL2H1(Apb1ClockHz/400'000)
		<< I2c::PecPosition	::Current
		<< I2c::Acknowledge::Disable
		<< I2c::StartGeneration::Disable
		<< I2c::StopGeneration::Disable
		<< I2c::ClockStretching::Disable
		<< I2c::GeneralCall::Disable
		<< I2c::PacketErrorCheck::Disable
		<< I2c::Arp::Disable
		<< I2c::Smbus::Disable
		<< I2c::DmaLast::Enable
		<< I2c::Dma::Enable
		<< I2c::InterruptBuffer::Disable
		<< I2c::InterruptEvent::Enable
		<< I2c::InterruptError::Enable
		<< I2c::SetClockFrequency(Apb1ClockHz)
		<< I2c::Set7BitAddress1(I2C_ADRESS)
		<< I2c::GeneralCall::Disable
	);
	i2cMs.Enable();
	I2C_SetOver(i2cMs, I2c::Init() << I2c::Acknowledge::Enable);
}

extern TaskHandle_t adcConvertTask;
void I2cMasterTaskFunc(void* pvParameters)
{
	I2cInit();
	Nvic::SetPriority(EXTI9_5_IRQn, 5, 0);
	NVIC_EnableIRQ(EXTI9_5_IRQn);
	LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_6);
	LL_EXTI_EnableFallingTrig_0_31(LL_EXTI_LINE_6);

	arrTxBufferMs[0] = 0b01;
	// high
	arrTxBufferMs[1] = CONFIG_H_MUX_0and1 | CONFIG_H_RANGE_256mV | CONFIG_H_MODE_CONTINIOUS;
	//arrTxBufferMs[1] = 0b1'000'001'0;
	arrTxBufferMs[2] = CONFIG_L_RATE_8sps;
	I2cMasterTx(3, I2C_ADRESS);

//Hi trash
	arrTxBufferMs[0] = 3;
	// high
	arrTxBufferMs[1] = 1<<7;
	// low
	arrTxBufferMs[2] = 0;
	I2cMasterTx(3, I2C_ADRESS);

//lo trash
	arrTxBufferMs[0] = 2;
	// high
	arrTxBufferMs[1] = 0;
	// low
	arrTxBufferMs[2] = 0;
	I2cMasterTx(3, I2C_ADRESS);

// read data
	arrTxBufferMs[0] = 0;
	I2cMasterTx(1, I2C_ADRESS);

	uint16_t address = 0;

	while(1)
	{
		uint32_t notifyFlag;
		xTaskNotifyWait(I2C_NOTIFY_NONE, I2C_NOTIFY_ALL, &notifyFlag, portMAX_DELAY);

		I2cMasterRx(2, I2C_ADRESS);

		int16_t adcData = (arrRxBufferMs[0]<<8U|arrRxBufferMs[1]);

		int64_t x = (int64_t)(256000*2*10) * (int64_t)adcData;
		int64_t curr64 = x*(int64_t)1000/(int64_t)0x10000;
		int32_t curr32 = curr64;
		//printf("uuuA%d\n", curr64);//for release

		static char txData[32];
		static int32_t test = -150;
		if(test>150) test = -150;
		else test++;
		//uint32_t sizeTx = sprintf(txData, "uA%d\n", test*1000*1000);//for test
		uint32_t sizeTx = sprintf(txData, "@@@_uA%d\n", curr32);//for release
		LOG_DEBUG("uuA%d", curr32);

		vTaskSuspendAll();
			xMessageBufferSend(uartTxMessageBuffer, txData, sizeTx, 0);
		xTaskResumeAll();
		vTaskDelay(1);
		BlueLed2.ResetPin();
		//if(adcConvertTask) xTaskNotify(adcConvertTask, ADC_NOTIFY_VOLT, eSetBits);
	}
}

