#include "AdcHandle.h"

#include "config.h"
#include "stm32f1xx_ll_adc.h"

#define LOG_MODULE_NAME adc
#if defined(LOG_LEVEL_ADC)
	#define LOG_MODULE_LEVEL LOG_LEVEL_ADC
#endif
#include "log_utils.h"

static void AdcInit()
{
	constexpr auto ADC_SPEED = LL_ADC_CHANNEL_4;
	constexpr auto ADC_VOLT = LL_ADC_CHANNEL_5;
	const auto& ADC_NUM = ADC1;

	Nvic::SetPriority(ADC1_2_IRQn, 5, 0);
	NVIC_EnableIRQ(ADC1_2_IRQn);

	LL_ADC_EnableIT_AWD1(ADC_NUM);
	LL_ADC_Enable(ADC_NUM);
	vTaskDelay(1);
	LL_ADC_StartCalibration(ADC_NUM);
	while(LL_ADC_IsCalibrationOnGoing(ADC_NUM)) vTaskDelay(1);
	LL_ADC_Disable(ADC_NUM);

	LL_ADC_SetChannelSamplingTime(ADC_NUM, ADC_SPEED, LL_ADC_SAMPLINGTIME_1CYCLE_5);//+12.5
	LL_ADC_SetChannelSamplingTime(ADC_NUM, ADC_VOLT, LL_ADC_SAMPLINGTIME_1CYCLE_5);//+12.5
	LL_ADC_REG_SetContinuousMode(ADC_NUM, LL_ADC_REG_CONV_CONTINUOUS);
	LL_ADC_INJ_SetTrigAuto(ADC_NUM, LL_ADC_INJ_TRIG_FROM_GRP_REGULAR);
	LL_ADC_INJ_SetTriggerSource(ADC_NUM, LL_ADC_INJ_TRIG_SOFTWARE);
	LL_ADC_SetSequencersScanMode(ADC_NUM, LL_ADC_SEQ_SCAN_ENABLE);
	LL_ADC_INJ_SetSequencerLength(ADC_NUM, LL_ADC_INJ_SEQ_SCAN_ENABLE_2RANKS);
	LL_ADC_INJ_SetSequencerRanks(ADC_NUM, LL_ADC_INJ_RANK_1, ADC_SPEED);
	LL_ADC_INJ_SetSequencerRanks(ADC_NUM, LL_ADC_INJ_RANK_2, ADC_VOLT);

	LL_ADC_SetAnalogWDMonitChannels(ADC_NUM, LL_ADC_AWD_CHANNEL_4_INJ);
	//LL_ADC_SetAnalogWDMonitChannels(ADC_NUM, LL_ADC_AWD_ALL_CHANNELS_INJ);
}

void AdcConvertTaskFunc(void* pvParameters)
{
	AdcInit();
	LL_ADC_Enable(ADC1);
	LL_ADC_INJ_StartConversionSWStart(ADC1);
	LL_ADC_SetAnalogWDThresholds(ADC1, LL_ADC_AWD_THRESHOLD_LOW, 0);
	LL_ADC_SetAnalogWDThresholds(ADC1, LL_ADC_AWD_THRESHOLD_HIGH, 120);
	//auto lastWakeTime = xTaskGetTickCount();
	auto time = xTaskGetTickCount();
	//bool isLowWd = true;
	static uint32_t i = 0;
	for(;;)
	{
		//
		static char txData[32];
		uint32_t notifyFlag = ADC_NOTIFY_NONE;
		xTaskNotifyWait(ADC_NOTIFY_NONE, ADC_NOTIFY_ALL, &notifyFlag, portMAX_DELAY);
		if(notifyFlag&ADC_NOTIFY_AWD)
		{
			if(i >= 1)
			{
				i = 0;
				uint32_t period = xTaskGetTickCount() - time;
				if (period>3)
				{
					uint32_t speedMms = 292'000/period;
					uint32_t speedMs = speedMms/1000;
					uint32_t speedKmh = 292'000*36/period/10/1000;
					//printf("\n%sPeriod:%d, Speed:%dm/s, %dkm/h, ADC:%d.\n", RTT_CTRL_TEXT_GREEN, period, speedMs, speedKmh, ADC1->JDR1);
					//static char txData[32];
					uint32_t sizeTx = sprintf(txData, "@@@_mm/sec%d\n", speedMms);
					vTaskSuspendAll();
						xMessageBufferSend(uartTxMessageBuffer, txData, sizeTx, 0);
					xTaskResumeAll();
				}
				else
				{
					vTaskDelay(2);
				}
				time = xTaskGetTickCount();
			}
			else
			{
				i++;
			}
		}
	}
}


void AdcVoltTaskFunc(void* pvParameters)
{
	auto lastWakeTime = xTaskGetTickCount();
	for(;;)
	{
		static char txData[32];
		uint32_t volt;
		volt = LL_ADC_INJ_ReadConversionData32(ADC1, LL_ADC_INJ_RANK_2);
		volt = volt * 3300 * 16 / 0xFFF;
		//uint32_t volt = LL_ADC_INJ_ReadConversionData32(ADC1, LL_ADC_INJ_RANK_2);
		uint32_t sizeTx = sprintf(txData, "@@@_mV%d\n", volt);
		LOG_INFO("mV: %d", volt);
		vTaskSuspendAll();
			xMessageBufferSend(uartTxMessageBuffer, txData, sizeTx, 0);
		xTaskResumeAll();
		//BlueLed1.InvertPin();
		vTaskDelayUntil(&lastWakeTime, 50);
	}
}

