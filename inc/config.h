
#ifndef __CONFIG_H
#define __CONFIG_H

#include "freertos_utils.h"
#include "ALX_USART.hpp"
#include "ALX_I2C.hpp"
#include "stm32f1xx_alx_gpio.hpp"
#include "stm32f1xx_alx_rcc.hpp"
#include "stm32f1xx_alx_dma.hpp"

//TODO
#include <stdio.h>
#include "SEGGER_RTT.h"

extern MessageBufferHandle_t uartTxMessageBuffer;

constexpr auto DMA_UartRx_IRQn = DMA1_Channel5_IRQn;
static auto& DmaUartRx = Dma1_Channel5;

constexpr auto DMA_UartTx_IRQn = DMA1_Channel4_IRQn;
static auto& DmaUartTx = Dma1_Channel4;

static auto& i2cMs = I2c1;
static auto& dmaI2cMasterTx = Dma1_Channel6;
static auto& dmaI2cMasterRx = Dma1_Channel7;

constexpr uint32_t I2C_BUFFER_SIZE_RX = 5;
constexpr uint32_t I2C_BUFFER_SIZE_TX = 5;

constexpr uint32_t AhbClockHz  = 72000000U;
constexpr uint32_t Apb1ClockHz = 72000000U/4;
constexpr uint32_t Apb1TimHz   = Apb1ClockHz*2;
constexpr uint32_t Apb2ClockHz = 72000000U;
constexpr uint32_t Apb2TimHz   = Apb2ClockHz;

extern Gpio::PinDef RedLed1;
extern Gpio::PinDef GreenLed1;
extern Gpio::PinDef BlueLed1;
extern Gpio::PinDef RedLed2;
extern Gpio::PinDef GreenLed2;
extern Gpio::PinDef BlueLed2;

#define LOG_LEVEL_I2C  5
#define LOG_LEVEL_UART 4
#define LOG_LEVEL_ADC  4

#endif /* __CONFIG_H */


