
#ifndef __UART_HANDLE_H
#define __UART_HANDLE_H


enum UartNotifyE
{
	UART_NOTIFY_NONE   = 0,
	UART_NOTIFY_TX     = 1,
	UART_NOTIFY_ALL    = 0xFFFF'FFFF
};

void UartTransferTaskFunc(void* pvParameters);

#endif /* __UART_HANDLE_H */