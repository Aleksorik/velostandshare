
#ifndef __ADCHANDLE_H
#define __ADCHANDLE_H


enum AdcNotifyE
{
	ADC_NOTIFY_NONE   = 0,

	ADC_NOTIFY_AWD    = 1<<0,
  ADC_NOTIFY_VOLT   = 1<<1,

	ADC_NOTIFY_ALL    = 0xFFFF'FFFF
};

void AdcConvertTaskFunc(void* pvParameters);
void AdcVoltTaskFunc(void* pvParameters);

#endif /* __ADCHANDLE_H */

