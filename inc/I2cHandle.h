
#ifndef __I2CHANDLE_H
#define __I2CHANDLE_H


extern uint8_t i2cAddress;

enum I2cNotifyE
{
	I2C_NOTIFY_NONE       = 0,
	I2C_NOTIFY_RX         = 1<<0,
	I2C_NOTIFY_TX         = 1<<1,
	I2C_NOTIFY_RX1        = 1<<2,
	I2C_NOTIFY_TX1        = 1<<3,
	I2C_NOTIFY_ADDRESS     = 1<<4,
	I2C_NOTIFY_ADDRESS_RX  = 1<<5,
	I2C_NOTIFY_ADDRESS_TX  = 1<<4,
	I2C_NOTIFY_TX_FINISHED= 1<<6,
	I2C_NOTIFY_START      = 1<<7,
	I2C_NOTIFY_STOP       = 1<<8,

	I2C_NOTIFY_BUS_ERR    = 1<<10,
	I2C_NOTIFY_ARBITRATION_LOST= 1<<11,
	I2C_NOTIFY_ACK_FAILED = 1<<12,
	I2C_NOTIFY_OURUN       = 1<<13,
	I2C_NOTIFY_PEC_ERR       = 1<<14,
	I2C_NOTIFY_TIMEOUT       = 1<<15,
	I2C_NOTIFY_ALERT       = 1<<16,

	GPIO_NOTIFY_ALERT           = 1<<17,

	I2C_NOTIFY_ELSE       = 1<<31,
	I2C_NOTIFY_ALL        = 0xFFFF'FFFF
};
enum class I2cStateE
{
	IDLE,
	RX,
	TX,
};

void I2cMasterTaskFunc(void* pvParameters);

#endif /* __I2CHANDLE_H */

